# Answer Digital Automation test
Author: David Hopcroft

Date:  April 5, 2021

Main work was done on the above date.  I did pull in some old code I wrote in the test/support directory.
However the tests written are all new and written for this test.

## limitations and assumptions
I have used the Spock specification framework to write the tests.
I have also split the tests into separate test classes one for each module of the automation test.
For brevity, I am only supporting Firefox for this test, as there was no specification otherwise.
I have not used java in a quite a while now, so this is most likely not the best code I have ever written.

Code dependencies are managed by maven, so the only dependencies I see are
 - having jave (>=8) installed and convigured
 - Maven installed was 3.6.3
 - the Selenium webdriver was used all the resources for this are stored in this project.

## Test Notes
The following sections contain my notes on the various test tasks.
### Practice Form = StudentRegistrationFormSpec.groovy
I split this in to 3 separate tests; Mandatory field entry, picture up-load and missing mandatory fields.
One main thing missing is the Page object model. If further work was to be done this would be my first "next step". The only reason I ommited it was time constraints and the simplicity of the pages.
*Mandatory field entry:* is a really simply written test once the form is entered and submitted it only checks that the head text is correct.
*Picture Upload:* is the same as the Madatory field but in this case the conformation table is parsed, placed in to a map, and the values for the image filename is verified.
*Missing Fields:* this tests fills in the first name and misses the surname.  Then checks the colour of the border of these input boxes to see if the field has been flagged as missing or not. 
### Alerts = AlertsSpec.groovy
Split in to two tests; show that the dialog appears after a period, and that the after clicking the accept button the dialog is no longer shown
### tool-tips = toolstipsSpec.groovy
Split into two tests one each for each control.
I had a problem using the prefered selenium method here.  I have left the none working code in the test.  My work around is a bit hacky but was to get the page source when the tool tip is shown and then simply look for the relative text in the page source.
### droppable = DragAndDropSpec.groovy
this is a simple single test to carry the drag and drop operation and check the destination state changed.
### modal dialog = ModalDialogSpec.groovy
Split into two tests the first verifies the modal dialog can be opened and the second that the dialog is removed when closed.
### date picker = DatePickerSpec.groovy
This is a single test class.  This test has not been completed.  The issue I have is that I have found the new date object on the calendar, but it is not accepting clicks to selec the date.
This test will need a LOT of verification due to all the possible date movements (28 day month to 31 and vice versa, with year change, time zone, …)