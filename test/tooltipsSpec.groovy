import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions
import spock.lang.Shared

import Support.SeleniumPropertySetup
import Support.BrowserInstance
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.NoAlertPresentException
import org.openqa.selenium.support.ui.WebDriverWait
import org.openqa.selenium.support.ui.ExpectedConditions
import spock.lang.Specification

class ToolTipsSpec  extends Specification {
    String form_url = "https://demoqa.com/tool-tips"
    @Shared
    WebDriver driver

    def setupSpec() {
        new SeleniumPropertySetup("3.141.59", false)
        driver = (WebDriver) new BrowserInstance("ff").getBrowserDriver()
    }

    def cleanupSpec() {
        driver.close()
    }

    def "Verify tool-tip over button is shown"() {
        given: "I browse to alert test page"
        driver.get(form_url)

        when: "I hover over button"
        WebElement we = driver.findElement(By.id("toolTipButton"))
        Actions builder = new Actions(driver)
        builder.moveToElement(we).build().perform()
        Thread.sleep(500)
        def page_text = driver.getPageSource()
        // TODO This is not working????:
        // WebElement toolTip = driver.findElement(By.xpath("//*[@id='buttonToolTip']/div[2]"))
        // String toolTipText = toolTip.getText()

        then: "I see the text of the tool-tip"
        page_text.contains("You hovered over the Button")


    }

    def "Verify tool-tip over text field is shown"() {
        given: "I browse to alerts test page"
        driver.get(form_url)

        when: "I hover over text field"
        WebElement we = driver.findElement(By.id("texFieldToolTopContainer"))
        Actions builder = new Actions(driver)
        builder.doubleClick(we).build().perform()
        Thread.sleep(500)
        def page_text = driver.getPageSource()

        then: "I see the text of the tool-tip"
        page_text.contains("You hovered over the text field")


    }

}

