package Support;

import Support.POM.BasePageElement;
import org.apache.log4j.Logger;
import org.openqa.selenium.InvalidArgumentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.concurrent.TimeUnit;


public class BrowserInstance {
    private WebDriver BrowserDriver;

    protected static final Logger log = Logger.getLogger(BasePageElement.class);

    public BrowserInstance(String name) {
        this.BrowserDriver = chooseBrowser(name, true);
    }

    public BrowserInstance(String name, boolean maximize) {
        this.BrowserDriver = chooseBrowser(name, maximize);
    }

    public WebDriver getBrowserDriver() {
        return this.BrowserDriver;
    }

    private WebDriver chooseBrowser(String name, boolean maximize) {
        WebDriver wd;
        String browserDriver;
        if (name.toLowerCase().equals("ff")
                || name.toLowerCase().equals("firefox")) {
            wd = configureFirefox();
            browserDriver = "Using Firefox: " + System.getProperty("webdriver.gecko.driver");
        } else {
            throw new InvalidArgumentException("Invalid browser choice: '" + name +"'");
        }
        log.info(browserDriver);

        if (maximize) {
            wd.manage().window().maximize();
        }
        return wd;
    }

    public void quit(){
        this.BrowserDriver.quit();
    }

    private WebDriver configureFirefox() {
        DesiredCapabilities caps  = DesiredCapabilities.firefox();

        return new FirefoxDriver(caps);
    }
}
