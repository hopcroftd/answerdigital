import spock.lang.Shared
import spock.lang.Specification

import Support.SeleniumPropertySetup
import Support.BrowserInstance
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import spock.lang.Specification

class StudentRegistrationFormSpec  extends Specification {
    String form_url = "https://demoqa.com/automation-practice-form"
    @Shared
    WebDriver driver


    def cleanupSpec() {
        driver.close()
    }

    def setupSpec() {
        new SeleniumPropertySetup("3.141.59", false)
        driver = (WebDriver) new BrowserInstance("ff").getBrowserDriver()
    }

    def "Verify Mandatory Field Entry"() {
        given: "I browse to Registration form"
        driver.get(form_url)

        when: "I enter all manditory fields and submit form"
        driver.findElement(By.id("firstName")).sendKeys("Fred")
        driver.findElement(By.id("lastName")).sendKeys("Flintstone")
        driver.findElement(By.xpath("//*[@id=\"genterWrapper\"]/div[2]/div[1]/label")).click()
        driver.findElement(By.id("userNumber")).sendKeys("0123456789")
        driver.findElement(By.id("submit")).click()

        then: "I expect to see the verification pop-up"
        WebElement we =  driver.findElement(By.xpath("/html/body/div[3]/div/div/div[1]"))
        we.text == "Thanks for submitting the form"
    }

    def "Verify Picture Upload"() {
        given: "I browse to Registration form"
        driver.get(form_url)

        when: "I enter all mandatory fields and and a picture then submit form"
        String picture_location = System.getProperty("user.dir") + "/src/main/resources/TestData/" + "Picture.jpg"
        driver.findElement(By.id("firstName")).sendKeys("Fred")
        driver.findElement(By.id("lastName")).sendKeys("Flintstone")
        driver.findElement(By.xpath("//*[@id=\"genterWrapper\"]/div[2]/div[1]/label")).click()
        driver.findElement(By.id("userNumber")).sendKeys("0123456789")
        driver.findElement(By.id("uploadPicture")).sendKeys(picture_location)
        driver.findElement(By.id("submit")).click();

        then: "I confirm picture has bee registered"
        def reported_form_data = [:]
        WebElement we = driver.findElement(By.xpath("/html/body/div[3]/div/div/div[2]/div/table/tbody"))
        for (WebElement table_row: we.findElements(By.tagName("tr"))) {
            def tuple = []
            for (WebElement row_field: table_row.findElements(By.tagName("td"))) {
                tuple.add(row_field.getText())
            }
            reported_form_data[tuple[0]] = tuple[1]
        }
        reported_form_data["Picture"] == "Picture.jpg"
        Thread.sleep(5000)

    }

    def "Verify mandatory omitted fields are flagged"() {
        given: "I browse to Registration form"
        driver.get(form_url)

        when: "I enter only one mandatory fields and submit form"
        driver.findElement(By.id("firstName")).sendKeys("Fred")
        driver.findElement(By.id("submit")).click()

        then: "I expect last name field to be red"
        String Missing_entry = driver.findElement(By.id("lastName")).getCssValue("border-top-color")
        Missing_entry == "rgb(220, 53, 69)"
        String Present_entry = driver.findElement(By.id("firstName")).getCssValue("border-top-color")
        Present_entry == "rgb(40, 167, 69)"
    }

}
