import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Action
import org.openqa.selenium.interactions.Actions
import spock.lang.Shared

import Support.SeleniumPropertySetup
import Support.BrowserInstance
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.NoAlertPresentException
import org.openqa.selenium.support.ui.WebDriverWait
import org.openqa.selenium.support.ui.ExpectedConditions
import spock.lang.Specification

class DragAndDropSpec  extends Specification {
    String form_url = "https://demoqa.com/droppable"
    @Shared
    WebDriver driver

    def setupSpec() {
        new SeleniumPropertySetup("3.141.59", false)
        driver = (WebDriver) new BrowserInstance("ff").getBrowserDriver()
    }

    def cleanupSpec() {
        driver.close()
    }

    def "Verify an item can be dragged on the page"() {
        given: "I browse to drag and drop test page"
        driver.get(form_url)

        when: "I drag the source object to the destination object"
        WebElement item = driver.findElement(By.id("draggable"))
        WebElement dropzone = driver.findElement(By.id("droppable"))
        Actions builder = new Actions(driver)
        Action dragAndDrop = builder.clickAndHold(item)
                .moveToElement(dropzone)
                .release(dropzone)
                .build()
                .perform()

        then: "I expect to see the destination has changed state"
        dropzone.getText() == "Dropped!"
    }

}
