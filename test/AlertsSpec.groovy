import spock.lang.Shared

import Support.SeleniumPropertySetup
import Support.BrowserInstance
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.NoAlertPresentException
import org.openqa.selenium.support.ui.WebDriverWait
import org.openqa.selenium.support.ui.ExpectedConditions
import spock.lang.Specification

class AlertsSpec  extends Specification {
    String form_url = "https://demoqa.com/alerts";
    @Shared
    WebDriver driver;

    def setupSpec() {
        new SeleniumPropertySetup("3.141.59", false)
        driver = (WebDriver) new BrowserInstance("ff").getBrowserDriver()
    }

    def cleanupSpec() {
        driver.close()
    }

    def "Verify Alert Appears After a Period"() {
        given: "I browse to alert test page"
        driver.get(form_url)

        when: "I click show button and after 5 seconds"
        driver.findElement(By.id("timerAlertButton")).click()

        then: "I expect to see alert after delay"
        // this will throw exception if Alert is not present
        new WebDriverWait(driver, 6)
                .ignoring(NoAlertPresentException.class)
                .until(ExpectedConditions.alertIsPresent())
        driver.switchTo().alert().accept() // tidy up not sure why
    }

    def "Verify alert can be dismissed by accepting it"() {
        given: "I browse to alerts test page"
        driver.get(form_url)

        when: "I click show after some time, and wait for alert to appear"
        driver.findElement(By.id("timerAlertButton")).click()
        new WebDriverWait(driver, 6)
                .ignoring(NoAlertPresentException.class)
                .until(ExpectedConditions.alertIsPresent())

        then: "I expect to see alert can be accepted, and is removed"
        driver.switchTo().alert().accept()
        ExpectedConditions.alertIsPresent() != null
    }

}
