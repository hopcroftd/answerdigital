import org.openqa.selenium.WebElement
import spock.lang.Shared

import Support.SeleniumPropertySetup
import Support.BrowserInstance
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.NoAlertPresentException
import org.openqa.selenium.support.ui.WebDriverWait
import org.openqa.selenium.support.ui.ExpectedConditions
import spock.lang.Specification

class ModalDialogsSpec  extends Specification {
    String form_url = "https://demoqa.com/modal-dialogs"
    @Shared
    WebDriver driver;

    def setupSpec() {
        new SeleniumPropertySetup("3.141.59", false)
        driver = (WebDriver) new BrowserInstance("ff").getBrowserDriver()
    }

    def cleanupSpec() {
        driver.close();
    }

    def "Verify Modal pop-up is shown"() {
        given: "I browse to modal dialog test page"
        driver.get(form_url)

        when: "I can open the Small Modal dialog"
        driver.findElement(By.id("showSmallModal")).click()

        then: "I expect the correct modal dialog is shown"
        WebElement we = driver.findElement(By.xpath('/html/body/div[3]/div/div/div[2]'))
        we.text.startsWith("This is a small modal")
    }

    def "Verify Modal pop-up can be closed"() {
        given: "I browse to modal dialog test page"
        driver.get(form_url)

        when: "I can open the Small Modal dialog, then close it"
        driver.findElement(By.id("showSmallModal")).click()
        driver.findElement(By.id('closeSmallModal')).click()
        WebElement modal_we = driver.findElement(By.xpath("/html/body/div[3]"))

        then: "I expect the modal dialog is not there"
        thrown org.openqa.selenium.NoSuchElementException
    }
}
