import org.openqa.selenium.WebElement
import spock.lang.Shared
import spock.lang.Specification

import Support.SeleniumPropertySetup
import Support.BrowserInstance
import org.openqa.selenium.*
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.support.ui.WebDriverWait
import org.openqa.selenium.support.ui.Select
import spock.lang.Specification
import groovy.time.TimeCategory

import java.time.Month

class DatePickerSpec  extends Specification {
    String form_url = "https://demoqa.com/date-picker"
    @Shared
    WebDriver driver

    def setupSpec() {
        new SeleniumPropertySetup("3.141.59", false)
        driver = (WebDriver) new BrowserInstance("ff").getBrowserDriver()
    }

    def cleanupSpec() {
        driver.close()
    }

    def "Verify Alert Appears After a Period"() {
        given: "I browse to date picker test page"
        driver.get(form_url)

        when: "find the new date 1 month ahead of current shown date"
        driver.findElement(By.id("datePickerMonthYearInput")).click()
        String dateShown = driver.findElement(By.id("datePickerMonthYearInput")).getAttribute('value')
        def dateFormat = "MM/dd/yy"
        TimeZone tz = TimeZone.getTimeZone("GMT");
        Date newPageDate
        use (TimeCategory) {
            Date PageDate = new Date().parse(dateFormat, dateShown)
            newPageDate = PageDate + 1.month
        }

        WebElement we;
        def monthIndex = (newPageDate.getAt(Calendar.MONTH)).toInteger()
        we = driver.findElement(By.xpath('//*[@id="datePickerMonthYear"]/div[2]/div[2]/div/div/div[2]/div[1]/div[2]/div[1]/select'))
        for (WebElement opt: we.findElements(By.tagName("option"))) {
            if (opt.getAttribute('value').equals(monthIndex.toString())) {
                opt.click()
            }
        }

        def yearIndex = newPageDate.getAt(Calendar.YEAR).toInteger()
        we = driver.findElement(By.xpath('//*[@id="datePickerMonthYear"]/div[2]/div[2]/div/div/div[2]/div[1]/div[2]/div[2]/select'))
        for (WebElement opt: we.findElements(By.tagName("option"))) {
            if (opt.getAttribute('value').equals(yearIndex.toString())) {
                opt.click()
            }
        }

        def dateinthemonth = newPageDate.getAt(Calendar.DAY_OF_MONTH).toString()
        String xpathExpr = "//*[contains(@class,'react-datepicker__week') and .//*[text()='${dateinthemonth}']]"
        we = driver.findElement(By.xpath(xpathExpr))
        for (WebElement day: we.findElements(By.tagName("div"))) {
            println day.getText()+","+ dateinthemonth
            if (day.getText().equals(dateinthemonth)) {
                day.click()  //TODO workout why click is not working...
            }
        }
        Thread.sleep(2000)

        then: "confirm that date of the form now matches newly calculated date"
        String ExpectedNewDateValue = "${newPageDate.getAt(Calendar.MONTH)}/${newPageDate.getAt(Calendar.DAY_OF_WEEK)}/${newPageDate.getAt(Calendar.YEAR)}"
        String dateNowShown = driver.findElement(By.id("datePickerMonthYearInput")).getAttribute('value')
        ExpectedNewDateValue == dateNowShown
    }
}
